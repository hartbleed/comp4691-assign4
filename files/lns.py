import random

from framework import Problem
import corner_heuristic
import argparse
import random as rand
from copy import deepcopy
rand.seed(0)

def destroy(prob: Problem, fraction_destroyed: float):
    """Destroy a fraction of the containers"""
    number_to_destroy = int(len(prob.conts) * fraction_destroyed)
    to_destroy = random.sample(prob.conts, number_to_destroy)
    for cont in to_destroy:
        boxes = cont.unpack_all()
        for b in boxes:
            prob.unpacked.append(b)


def repair(prob: Problem):
    """Repair solution using corner heuristic"""
    # rand.choices(diff_order_conts, weights=[1, 1, 15, 15, 15], k=1)[0],
    diff_order_boxes = [
        lambda x: sorted(x, key=lambda b: b.dims[0] * b.dims[1], reverse=True),
        lambda x: sorted(x, key=lambda b: b.dims[0] * b.dims[1], reverse=False),
        lambda x: sorted(x, key=lambda b: b.weight, reverse=True),
        lambda x: sorted(x, key=lambda b: b.weight, reverse=False),
        lambda x: rand.sample(x, len(x))
    ]
    diff_order_conts = [
        lambda x: sorted(x, key=lambda c: c.dims[0] * c.dims[1], reverse=True),
        lambda x: sorted(x, key=lambda c: c.dims[0] * c.dims[1], reverse=False),
        lambda x: sorted(x, key=lambda c: (c.dims[0] * c.dims[1]) - (sum([b.dims[0] * b.dims[1] for _, b in c.packed])), reverse=True),
        lambda x: sorted(x, key=lambda c: (c.dims[0] * c.dims[1]) - (sum([b.dims[0] * b.dims[1] for _, b in c.packed])),
                         reverse=False),
        lambda x: rand.sample(x, len(x))
    ]
    diff_order_corners = [
        lambda x: sorted(x, key=lambda c: c.pos[0] + c.pos[1], reverse=True),
        lambda x: sorted(x, key=lambda c: c.pos[0] + c.pos[1], reverse=False),
        lambda x: sorted(x, key=lambda c: c.pos[0], reverse=True),
        lambda x: sorted(x, key=lambda c: c.pos[0], reverse=False),
        lambda x: rand.sample(x, len(x))
    ]

    corner_heuristic.corner_heuristic(prob,
                                      order_boxes=rand.choices(diff_order_boxes, weights=[2, 15, 20, 2, 2], k=1)[0],
                                      order_conts=diff_order_conts[4],
                                      order_corners=rand.choices(diff_order_corners, weights=[20, 20, 2, 5, 2], k=1)[0],
                                      order_orients=lambda x: rand.sample(x, len(x)),
                                      )


def lns(prob_init: Problem,
        n_iters: int,
        fraction_destroyed: float):
    """The LNS algorithm"""
    best = prob_init
    for i in range(n_iters):
        new = deepcopy(best)
        destroy(new, fraction_destroyed)
        repair(new)
        if new.objective() >= best.objective():
            best = new
    return best


if __name__ == "__main__":
    par = argparse.ArgumentParser("2D Packing Large Neighbourhood Search")
    par.add_argument("file", help="json instance file")
    par.add_argument("--save-plot", default=None,
                     help="save plot to file")
    par.add_argument("--no-plot", action='store_true',
                     help="don't plot solution")

    args = par.parse_args()

    rand.seed(0)

    prob = Problem(args.file)
    # You can change the ordering functions and other parameters here:
    prob = lns(prob, n_iters=300, fraction_destroyed=0.25)
    print(prob.objective())
    if not args.no_plot:
        prob.plot(file_name=args.save_plot)
