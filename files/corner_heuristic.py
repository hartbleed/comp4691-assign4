from framework import Orientation, Problem, Box, Corner, Container
from typing import TypeVar, List, Callable
import argparse
import random as rand

def overlaps(cornerA: Corner, boxA: Box, cornerB: Corner, boxB: Box) -> bool:
    cornerA_pos = cornerA.pos
    cornerB_pos = cornerB.pos
    if cornerA_pos[0] >= cornerB_pos[0] and cornerB_pos[0] + boxB.orientated()[0] > cornerA_pos[0]:
        if cornerA_pos[1] >= cornerB_pos[1] and cornerB_pos[1] + boxB.orientated()[1] > cornerA_pos[1]:
            return True
        if cornerB_pos[1] >= cornerA_pos[1] and cornerA_pos[1] + boxA.orientated()[1] > cornerB_pos[1]:
            return True
    if cornerB_pos[0] >= cornerA_pos[0] and cornerA_pos[0] + boxA.orientated()[0] > cornerB_pos[0]:
        if cornerA_pos[1] >= cornerB_pos[1] and cornerB_pos[1] + boxB.orientated()[1] > cornerA_pos[1]:
            return True
        if cornerB_pos[1] >= cornerA_pos[1] and cornerA_pos[1] + boxA.orientated()[1] > cornerB_pos[1]:
            return True
    return False

def check_fit(cont: Container, corner: Corner, box: Box) -> bool:
    """True if box (with orientation) fits at particular corner in container"""
    assert(corner in cont.corners)
    # Check if it fits inside the Container
    dims = box.orientated()
    if dims[0] + corner.pos[0] > cont.dims[0] or dims[1] + corner.pos[1] > cont.dims[1]:
        return False
    # Check if it overlaps with other Boxes
    for c, b in cont.packed:
        if overlaps(corner, box, c, b):
            return False
    return True

T = TypeVar('T')
OrderFn = Callable[[List[T]], List[T]]  # Generic ordering function type

def try_pack(box: Box,
             prob: Problem,
             order_conts: OrderFn[Container]=lambda x: x,
             order_corners: OrderFn[Corner]=lambda x: x,
             order_orients: OrderFn[Orientation]=lambda x: x
             ):
    orientations = [Orientation.HORIZONTAL, Orientation.VERTICAL]
    for container in order_conts(prob.conts):
        for corner in order_corners(container.corners):
            for orientation in order_orients(orientations):
                box.orient = orientation
                if check_fit(container, corner, box):
                    prob.unpacked.remove(box)
                    container.pack(corner, box)
                    return prob
    return prob

def corner_heuristic(prob: Problem,
                     order_boxes: OrderFn[Box]=lambda x: x,
                     order_conts: OrderFn[Container]=lambda x: x,
                     order_corners: OrderFn[Corner]=lambda x: x,
                     order_orients: OrderFn[Orientation]=lambda x: x,
                     ):
    """The corner heuristic algorithm"""
    # If the order is being passed to the function, just take the first from each list
    boxes = order_boxes(prob.unpacked.copy())
    for box in boxes:
        prob = try_pack(box, prob, order_conts, order_corners, order_orients)



if __name__ == "__main__":
    par = argparse.ArgumentParser("2D Packing Corner Heuristic Solver")
    par.add_argument("file", help="json instance file")
    par.add_argument("--save-plot", default=None,
                     help="save plot to file")
    par.add_argument("--no-plot", action='store_true',
                     help="don't plot solution")

    args = par.parse_args()

    rand.seed(0)

    prob = Problem(args.file)
    # Play around with the ordering functions here in Q1.3:
    corner_heuristic(prob,
                     order_boxes=lambda x: sorted(x, key=lambda b: b.dims[0] * b.dims[1], reverse=True),
                     order_conts=lambda x: x,
                     order_corners=lambda x: sorted(x, key=lambda c: c.pos[0] + c.pos[1], reverse=False),
                     order_orients=lambda x: x,
                     )
    print(prob.objective())
    if not args.no_plot:
        prob.plot(file_name=args.save_plot)

